

function map(input_array=[], cb){

    if (input_array === [] || !Array.isArray(input_array) || input_array.length == 0){
        return [];
    }
    if (cb === undefined || typeof(cb) !== 'function'){
        return input_array;
    }
    

    let result_array = [];

    for(let index = 0; index < input_array.length; index++){
        
        let element = input_array[index];
        
        // console.log(index);

        let temp = cb(element, index, input_array);

        result_array.push(temp);

    }
    return result_array;

}

module.exports = map;
