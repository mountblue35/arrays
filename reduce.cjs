

function reduce(input_array=[], cb, startingValue){

    if (input_array === [] || !Array.isArray(input_array) || input_array.length == 0 || input_array === undefined){
        return [];
    }
    if (cb === undefined || typeof(cb) !== 'function'){
        return input_array;
    }

    let flag = true;        // To see if startingValue is passed or not

    if (typeof(startingValue) === 'undefined'){
        
        startingValue = input_array[0];
        flag = false;
    }
    for(let index = 0; index < input_array.length; index++){
        if (flag === false){
            flag = true;
            continue;
        }
        let element = input_array[index];
        startingValue = cb(startingValue, element, index, input_array);

    }
    return startingValue;

}

module.exports = reduce;
