

function each(input_array=[], cb){

    if (input_array === []){
        return [];
    }
    
    for(let index in input_array){
        // console.log(index);
        input_array[index] = cb(input_array, index);

    }
    return input_array;

}



module.exports = each;

