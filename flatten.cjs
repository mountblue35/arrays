function flatten(input_array=[], depth = 1){

    if (input_array === []){
        return [];
    }
    let result = [];

    function flat(ele, depth){

        for(let index in ele){

            let value = ele[index];
            
            if (Array.isArray(value) === true){
                if(depth > 0){
                    flat(value, depth-1);
                }
                else{
                    result.push(value);
                }
            }
            else{
                result.push(value);
            }
        }
        
    }
    flat(input_array, depth);
    
    return result;
    
}

module.exports = flatten;