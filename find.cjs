


function find(input_array=[], cb){

    if (input_array === []){
        return [];
    }
    let result ;
    for(let index in input_array){
        result = cb(input_array[index]);
        if (result === true){
            return input_array[index];
        }
    }
    return undefined;

}

module.exports = find;
