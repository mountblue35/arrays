



function filter(input_array, cb){

    if (input_array === [] || !Array.isArray(input_array) || input_array.length == 0 || input_array === undefined){
        return [];
    }
    if (cb === undefined || typeof(cb) !== 'function'){
        return input_array;
    }
    let result = [];
    for(let index = 0; index < input_array.length; index++){
        
        let element = input_array[index];

        let temp = cb(element, index, input_array);
        
        if (temp === true ){
            
            result.push(element);
        }
    }
    
    if (result.length >= 1){
        return result;
    }else{
        
    return [];   
    }
}

module.exports = filter;
